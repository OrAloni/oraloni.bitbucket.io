---
title: "Big Data BIU - project"
author: "Or Aloni and Ofir Blum"
subtitle: Checking the connection between players' phisical properties to their preformance
  in the NBA.
output:
  html_document:
    keep_md: yes
  pdf_document: default
---

*Functions for later use*

```r
#rescaling the data after predicting
rescale<-function(scaleData,lambda,min,sd,mean)
{
  #inverse boxcox
  for (q in 1:length(lambda)) {
    if (lambda[q] == 0)
    {
       scaleData[,q]<-exp(scaleData[,q])
    }
    else 
    {
       scaleData[,q]<-(lambda[q]*scaleData[,q] + 1)^(1/lambda[q])
    }
    scaleData[,q]<-(scaleData[,q]+min[q]-1)*sd[q]+mean[q]
  }
  return(scaleData)
}

scaleD<-function(data,lambda,min,sd,mean){
    data<-((data-mean)/sd)-min+1
    #data[,i]<-boxcox(data[,i],lambda = lambda[i])
    if(lambda==0){
      data<-log(data)
    }
    else{
      data<-(data^lambda-1)/lambda
    }
        
  }
```






```r
require(dplyr)
require(MLmetrics)
require(base)
require(fastDummies)
require(stats)
require(plyr)
require(ggplot2)
require(EnvStats)
require(RColorBrewer)
require(corrplot)
require(caret)
require(leaps)
background<-brewer.pal(8,"Pastel2")
players<-read.csv("all_seasons.csv")
```


1. **Introduction to the dataset**:
We found our dataset in the kaggle website (see part 5).
2. **Research question** - 
We decided to check whether there is a connection between a player's characteristics (like: height, weight, origin country, draft round, etc) to his performance in the NBA. We want to build a model that will try to predict the player's game paremeters(like: points, rebounds, etc) according to his phisical properties and other characteristics .   

3. **What are the Observations in the data**
The observation in the data is information about players in the NBA

```r
#original dataset dimension
dim(players)
## [1] 9561   22
```

**data preprocessing**

```r
players<-na.omit(players)
#choosing only the drafted players
df<-subset(players,players$draft_year!="Undrafted",select =c('player_name',colnames(players)[12:21]))
draft<-players[!duplicated(players$player_name),1:21]
draft<-data.frame(draft$player_name,draft[,c(1,3:21)])
colnames(draft)[1]<-"player_name"
draft<-subset(draft,draft$draft_year!="Undrafted"||draft$draft_number=="Undrafted"||draft$draft_round=="Undrafted",select =colnames(draft))
#calculating for every player his stats' mean
df = draft[FALSE,]
for(i in levels(draft$player_name))
{
  df[i,1]<-i
  df[i,2]<-mean(draft$gp[draft$player_name==i])
  df[i,3]<-mean(draft$pts[draft$player_name==i])
  df[i,4]<-mean(draft$reb[draft$player_name==i])
  df[i,5]<-mean(draft$ast[draft$player_name==i])
  df[i,6]<-mean(draft$net_rating[draft$player_name==i])
  df[i,7]<-mean(draft$oreb_pct[draft$player_name==i])
  df[i,8]<-mean(draft$dreb_pct[draft$player_name==i])
  df[i,9]<-mean(draft$usg_pct[draft$player_name==i])
  df[i,10]<-mean(draft$ts_pct[draft$player_name==i])
  df[i,11]<-mean(draft$ast_pct[draft$player_name==i])
  
}
df<-filter(df,!is.nan(df$gp))
data<-inner_join(df,draft,by="player_name")[,c(1,25:41)]
colnames(data)<-colnames(players)[c(2,5:21)]
data$draft_year<-as.numeric(paste(data$draft_year))
data$draft_round<-as.numeric(paste(data$draft_round))
data$draft_number<-as.numeric(paste(data$draft_number))
data<-filter(data,!is.na(data$draft_year))
data<-filter(data,!is.na(data$draft_round))
data<-filter(data,!is.na(data$draft_number))
dim(data)
## [1] 1403   18
seasons<-as.data.frame(table(players$player_name))
colnames(seasons)[1]<-"player_name"
data<-inner_join(data,seasons)
colnames(data)[19]<-"seasons_played"
```
We began with 9561 rows but after filtering we finished with 1403 rows.

4. **What is the structure of the database?** What are the columns? What do they stand for?

our data has 19 columns which are:


```r
colnames(data)
##  [1] "player_name"    "player_height"  "player_weight" 
##  [4] "college"        "country"        "draft_year"    
##  [7] "draft_round"    "draft_number"   "gp"            
## [10] "pts"            "reb"            "ast"           
## [13] "net_rating"     "oreb_pct"       "dreb_pct"      
## [16] "usg_pct"        "ts_pct"         "ast_pct"       
## [19] "seasons_played"
```
A simple explanation for each column:

player_height- The player's height

player_weight- The player's weight

college- The college the player played for.

country-Name of the country the player was born in (not necessarily the nationality)

draft_year- The year the player was drafted

draft_round- The draft round the player was picked

draft_number- The number at which the player was picked in his draft round

gp- Games played throughout the season

pts- Average number of points scored

reb- Average number of rebounds grabbed

ast- Average number of assists distributed

net_rating- Team's point differential per 100 possessions 
while the player is on the court

oreb_pct- Percentage of available offensive rebounds the player grabbed while he was on the floor

dreb_pct- Percentage of available defensive rebounds the player grabbed while he was on the floor

usg_pct- Percentage of team plays used by the player while he was on the floor (FGA + Possession Ending FTA + TO) / POSS)

ts_pct- Measure of the player's shooting efficiency that takes into account free throws, 2 and 3 point shots (PTS / (2x(FGA + 0.44 x FTA)))

ast_pct- Percentage of teammate field goals the player assisted while he was on the floor

seasons_played- number of seasons the player played



5. **Data Source:**  https://www.kaggle.com/justinas/nba-players-data
6. **Relevant summary statistics** 

```r
myColors<-brewer.pal(9,"Reds")
#histograms for every column
for (i in c(2:19) ){
  hist(as.numeric(data[,i]), main=colnames(data)[i],probability = F,col = myColors[5],border = myColors[7])
}
```

<div class="rimage center"><img src="fig/unnamed-chunk-7-1.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-2.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-3.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-4.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-5.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-6.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-7.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-8.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-9.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-10.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-11.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-12.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-13.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-14.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-15.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-16.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-17.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-7-18.png" class="plot" /></div>


```r
cordata<-cor(data[,c(2,3,6:19)])
corrplot(cordata,method = 'number')
```

<div class="rimage center"><img src="fig/unnamed-chunk-8-1.png" class="plot" /></div>
We would like to normalize the data using boxcox transformation. In order to do so, we need to make our data positive (and save the shift we did to each parameter so we could reverse it).

```r
dataMean<-apply(data[,c(2,3,6:19)],2,mean)
dataSD<-apply(data[,c(2,3,6:19)],2,sd)
scaleData<-as.data.frame(scale(data[,c(2,3,6:19)]))
minData<-apply(scaleData,2,min)
for (i in 1:length(minData)) {
  scaleData[i]<-scaleData[i]-minData[i]
}
```
We have decided to make it a little bit more easier by replacing the origin country with the origin continent since most of the players came from the USA. If we don't do that we would have way to many parameters, so the model building would be much harder.



```r
b<-1:69
data$continent<-data$country
  b[c(1,8,64,68)]<-"South_America"
  
 b[c(2,40)]<-"Oceania"
  
 b[c(3,6,7,14,15,18,19,20,23,25,26,29,31,33,34,35,38,39,43,45,46,48,49,50,53,56,57,62,63,67,69)]<-"Europe"
  
 b[c(4,11,37,61,65,66)]<-"North_America"
  
 b[c(5,17,27,32,42,44,54)]<-"Central_America"
  
 b[c(9,10,13,16,21,24,36,41,47,52,55,58,59)]<-"Africa"
  
 b[c(12,28,30,51)]<-"Asia"
  
 b[c(22,60)]<-"Eurasia"
 
 levels(data$continent)<-b
categorialData<-dummy_cols(dplyr::select(data,continent))[,2:9]
```
**model selection **
Normalizing the data using boxcox transformation

```r
scaleData<-scaleData+1
box<-apply(scaleData, 2,boxcox,plotit=T)
#box
#lapply(box,plot)
lambda<-1:16
for (i in 1:16) {
lambda[i]<-box[[i]]$lambda[box[[i]]$objective==max(box[[i]]$objective)]
scaleData[,i]<-boxcoxTransform(scaleData[,i],lambda = lambda[i])
}
for (i in c(1:16) ){
  hist(as.numeric(scaleData[,i]), main=colnames(scaleData)[i],probability = F,col = myColors[5],border = myColors[7])
}
```

<div class="rimage center"><img src="fig/unnamed-chunk-11-1.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-2.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-3.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-4.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-5.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-6.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-7.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-8.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-9.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-10.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-11.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-12.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-13.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-14.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-15.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-11-16.png" class="plot" /></div>

```r

smp_size <- floor(0.75 * nrow(data))

set.seed(123)
train_ind <- sample(seq_len(nrow(data)), size = smp_size)


train <-data.frame(scaleData[train_ind, ],categorialData[train_ind,])
test <-data.frame(scaleData[-train_ind, ],categorialData[-train_ind,])
train$college<-NULL
train$country<-NULL
test$college<-NULL
test$country<-NULL
trainData<-train[,c(1:5,17:24)]
```
First of all we would like to test for each parameter the model that includes all the predictors' first degree.

```r

knownColumn<-c(1:5,17:280)
unknownColumn<-6:16

gp1<-lm(data=trainData,train$gp~.)
summary(gp1)
## 
## Call:
## lm(formula = train$gp ~ ., data = trainData)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.9698 -1.2070  0.0266  1.1498  3.8958 
## 
## Coefficients: (1 not defined because of singularities)
##                           Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                5.91228    0.80187   7.373  3.4e-13 ***
## player_height             -0.12952    0.03595  -3.603 0.000329 ***
## player_weight              0.26705    0.16395   1.629 0.103655    
## draft_year                -0.05625    0.02075  -2.711 0.006815 ** 
## draft_round                0.34445    0.16480   2.090 0.036849 *  
## draft_number              -2.02929    0.19686 -10.308  < 2e-16 ***
## continent_North_America   -0.80554    0.73429  -1.097 0.272886    
## continent_Central_America -0.77530    0.91373  -0.849 0.396352    
## continent_Europe          -0.49444    0.74677  -0.662 0.508048    
## continent_South_America   -0.30470    0.86477  -0.352 0.724653    
## continent_Oceania         -1.74454    0.94313  -1.850 0.064634 .  
## continent_Africa          -0.86236    0.79444  -1.085 0.277956    
## continent_Eurasia          0.15006    0.86141   0.174 0.861742    
## continent_Asia                  NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.453 on 1039 degrees of freedom
## Multiple R-squared:  0.2079,	Adjusted R-squared:  0.1987 
## F-statistic: 22.72 on 12 and 1039 DF,  p-value: < 2.2e-16

pts1<-lm(data=trainData,train$pts~.)
summary(pts1)
## 
## Call:
## lm(formula = train$pts ~ ., data = trainData)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -0.6454 -0.1475  0.0025  0.1484  0.6786 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                1.309103   0.118803  11.019  < 2e-16 ***
## player_height             -0.025878   0.005326  -4.859 1.36e-06 ***
## player_weight              0.027155   0.024291   1.118   0.2639    
## draft_year                -0.014405   0.003074  -4.686 3.15e-06 ***
## draft_round                0.109839   0.024416   4.499 7.61e-06 ***
## draft_number              -0.443747   0.029167 -15.214  < 2e-16 ***
## continent_North_America   -0.148002   0.108791  -1.360   0.1740    
## continent_Central_America -0.106784   0.135376  -0.789   0.4304    
## continent_Europe          -0.109655   0.110639  -0.991   0.3219    
## continent_South_America   -0.083198   0.128123  -0.649   0.5162    
## continent_Oceania         -0.294486   0.139731  -2.108   0.0353 *  
## continent_Africa          -0.162023   0.117703  -1.377   0.1690    
## continent_Eurasia         -0.136766   0.127624  -1.072   0.2841    
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.2153 on 1039 degrees of freedom
## Multiple R-squared:  0.3353,	Adjusted R-squared:  0.3276 
## F-statistic: 43.68 on 12 and 1039 DF,  p-value: < 2.2e-16

reb1<-lm(data=trainData,train$reb~.)
summary(reb1)
## 
## Call:
## lm(formula = train$reb ~ ., data = trainData)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -0.79025 -0.13683  0.00904  0.15071  0.64923 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                0.607814   0.116257   5.228 2.07e-07 ***
## player_height              0.005561   0.005211   1.067 0.286229    
## player_weight              0.141449   0.023770   5.951 3.65e-09 ***
## draft_year                -0.011613   0.003008  -3.861 0.000120 ***
## draft_round                0.082193   0.023893   3.440 0.000605 ***
## draft_number              -0.349944   0.028542 -12.261  < 2e-16 ***
## continent_North_America   -0.004066   0.106460  -0.038 0.969544    
## continent_Central_America  0.104070   0.132475   0.786 0.432291    
## continent_Europe           0.005458   0.108268   0.050 0.959803    
## continent_South_America    0.013824   0.125377   0.110 0.912224    
## continent_Oceania         -0.260372   0.136737  -1.904 0.057163 .  
## continent_Africa           0.021298   0.115181   0.185 0.853337    
## continent_Eurasia          0.066189   0.124889   0.530 0.596237    
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.2107 on 1039 degrees of freedom
## Multiple R-squared:  0.3422,	Adjusted R-squared:  0.3346 
## F-statistic: 45.04 on 12 and 1039 DF,  p-value: < 2.2e-16

ast1<-lm(data=trainData,train$ast~.)
summary(ast1)
## 
## Call:
## lm(formula = train$ast ~ ., data = trainData)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -0.45460 -0.11059  0.00283  0.11357  0.49399 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                1.315454   0.087726  14.995  < 2e-16 ***
## player_height             -0.042450   0.003932 -10.795  < 2e-16 ***
## player_weight             -0.065072   0.017936  -3.628   0.0003 ***
## draft_year                -0.015266   0.002270  -6.726 2.87e-11 ***
## draft_round                0.085702   0.018029   4.754 2.28e-06 ***
## draft_number              -0.282457   0.021537 -13.115  < 2e-16 ***
## continent_North_America   -0.168691   0.080333  -2.100   0.0360 *  
## continent_Central_America -0.143239   0.099963  -1.433   0.1522    
## continent_Europe          -0.109260   0.081697  -1.337   0.1814    
## continent_South_America   -0.092266   0.094607  -0.975   0.3297    
## continent_Oceania         -0.247175   0.103179  -2.396   0.0168 *  
## continent_Africa          -0.189987   0.086913  -2.186   0.0290 *  
## continent_Eurasia         -0.136171   0.094239  -1.445   0.1488    
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.159 on 1039 degrees of freedom
## Multiple R-squared:  0.4678,	Adjusted R-squared:  0.4616 
## F-statistic:  76.1 on 12 and 1039 DF,  p-value: < 2.2e-16

net_rating1<-lm(data=trainData,train$net_rating~.)
summary(net_rating1)
## 
## Call:
## lm(formula = train$net_rating ~ ., data = trainData)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -70.576  -4.212   0.535   4.921  70.927 
## 
## Coefficients: (1 not defined because of singularities)
##                           Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                78.0385     5.6302  13.861  < 2e-16 ***
## player_height              -0.6946     0.2524  -2.752  0.00602 ** 
## player_weight               1.9259     1.1511   1.673  0.09463 .  
## draft_year                 -0.4200     0.1457  -2.883  0.00402 ** 
## draft_round                -1.4469     1.1571  -1.250  0.21141    
## draft_number               -0.0516     1.3822  -0.037  0.97023    
## continent_North_America    -2.9515     5.1557  -0.572  0.56712    
## continent_Central_America  -0.8604     6.4156  -0.134  0.89335    
## continent_Europe           -0.9578     5.2433  -0.183  0.85510    
## continent_South_America    -5.3900     6.0719  -0.888  0.37491    
## continent_Oceania         -14.4057     6.6220  -2.175  0.02982 *  
## continent_Africa           -2.5773     5.5781  -0.462  0.64414    
## continent_Eurasia           0.2274     6.0482   0.038  0.97002    
## continent_Asia                  NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 10.2 on 1039 degrees of freedom
## Multiple R-squared:  0.0295,	Adjusted R-squared:  0.01829 
## F-statistic: 2.631 on 12 and 1039 DF,  p-value: 0.001801

oreb1<-lm(data=trainData,train$oreb_pct~.)
summary(oreb1)
## 
## Call:
## lm(formula = train$oreb_pct ~ ., data = trainData)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -1.44854 -0.17210  0.01273  0.17870  1.57499 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)               -0.523305   0.169642  -3.085  0.00209 ** 
## player_height              0.052166   0.007605   6.860 1.18e-11 ***
## player_weight              0.318693   0.034685   9.188  < 2e-16 ***
## draft_year                -0.009978   0.004389  -2.273  0.02321 *  
## draft_round               -0.066007   0.034864  -1.893  0.05860 .  
## draft_number               0.083832   0.041648   2.013  0.04439 *  
## continent_North_America    0.296500   0.155346   1.909  0.05658 .  
## continent_Central_America  0.515688   0.193307   2.668  0.00776 ** 
## continent_Europe           0.223951   0.157985   1.418  0.15662    
## continent_South_America    0.166473   0.182950   0.910  0.36307    
## continent_Oceania          0.162742   0.199526   0.816  0.41489    
## continent_Africa           0.324387   0.168071   1.930  0.05387 .  
## continent_Eurasia          0.420339   0.182238   2.307  0.02128 *  
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.3075 on 1039 degrees of freedom
## Multiple R-squared:  0.4331,	Adjusted R-squared:  0.4265 
## F-statistic: 66.15 on 12 and 1039 DF,  p-value: < 2.2e-16

dreb1<-lm(data=trainData,train$dreb_pct~.)
summary(dreb1)
## 
## Call:
## lm(formula = train$dreb_pct ~ ., data = trainData)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -2.3277 -0.2245 -0.0003  0.2455  3.3682 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                0.008452   0.247082   0.034   0.9727    
## player_height              0.069592   0.011076   6.283 4.87e-10 ***
## player_weight              0.367498   0.050519   7.275 6.84e-13 ***
## draft_year                 0.010454   0.006393   1.635   0.1023    
## draft_round               -0.003396   0.050780  -0.067   0.9467    
## draft_number              -0.116630   0.060660  -1.923   0.0548 .  
## continent_North_America    0.246784   0.226260   1.091   0.2757    
## continent_Central_America  0.489108   0.281550   1.737   0.0826 .  
## continent_Europe           0.285275   0.230103   1.240   0.2153    
## continent_South_America    0.269575   0.266465   1.012   0.3119    
## continent_Oceania         -0.379191   0.290608  -1.305   0.1922    
## continent_Africa           0.419424   0.244794   1.713   0.0869 .  
## continent_Eurasia          0.382352   0.265427   1.441   0.1500    
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.4478 on 1039 degrees of freedom
## Multiple R-squared:  0.3727,	Adjusted R-squared:  0.3655 
## F-statistic: 51.44 on 12 and 1039 DF,  p-value: < 2.2e-16

usg1<-lm(data=trainData,train$usg_pct~.)
summary(usg1)
## 
## Call:
## lm(formula = train$usg_pct ~ ., data = trainData)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -2.31514 -0.26406 -0.01342  0.26365  1.65899 
## 
## Coefficients: (1 not defined because of singularities)
##                             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                3.0121739  0.2356134  12.784  < 2e-16 ***
## player_height             -0.0324737  0.0105618  -3.075  0.00216 ** 
## player_weight             -0.0004096  0.0481736  -0.009  0.99322    
## draft_year                -0.0039852  0.0060960  -0.654  0.51342    
## draft_round               -0.0049104  0.0484226  -0.101  0.91925    
## draft_number              -0.2789238  0.0578440  -4.822 1.63e-06 ***
## continent_North_America   -0.1694628  0.2157572  -0.785  0.43238    
## continent_Central_America -0.2338608  0.2684813  -0.871  0.38393    
## continent_Europe          -0.1048429  0.2194222  -0.478  0.63288    
## continent_South_America   -0.0786301  0.2540959  -0.309  0.75704    
## continent_Oceania         -0.0161004  0.2771184  -0.058  0.95368    
## continent_Africa          -0.2362616  0.2334314  -1.012  0.31171    
## continent_Eurasia         -0.2176321  0.2531069  -0.860  0.39007    
## continent_Asia                    NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.427 on 1039 degrees of freedom
## Multiple R-squared:  0.09178,	Adjusted R-squared:  0.08129 
## F-statistic: 8.749 on 12 and 1039 DF,  p-value: 4.449e-16

ts1<-lm(data=trainData,train$ts_pct~.)
summary(ts1)
## 
## Call:
## lm(formula = train$ts_pct ~ ., data = trainData)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -9.4046 -0.8702  0.2084  1.0927 16.8948 
## 
## Coefficients: (1 not defined because of singularities)
##                           Estimate Std. Error t value Pr(>|t|)    
## (Intercept)               10.34682    1.27434   8.119 1.32e-15 ***
## player_height             -0.14427    0.05712  -2.526  0.01170 *  
## player_weight              0.60284    0.26055   2.314  0.02088 *  
## draft_year                -0.04792    0.03297  -1.453  0.14640    
## draft_round                0.28086    0.26190   1.072  0.28380    
## draft_number              -0.86608    0.31286  -2.768  0.00573 ** 
## continent_North_America   -0.59798    1.16695  -0.512  0.60846    
## continent_Central_America -0.02952    1.45211  -0.020  0.98378    
## continent_Europe          -0.07587    1.18677  -0.064  0.94904    
## continent_South_America   -0.33535    1.37430  -0.244  0.80727    
## continent_Oceania         -1.11847    1.49882  -0.746  0.45570    
## continent_Africa          -0.39747    1.26254  -0.315  0.75296    
## continent_Eurasia         -0.41403    1.36896  -0.302  0.76237    
## continent_Asia                  NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 2.31 on 1039 degrees of freedom
## Multiple R-squared:  0.02171,	Adjusted R-squared:  0.01042 
## F-statistic: 1.922 on 12 and 1039 DF,  p-value: 0.02842

ast_pct1<-lm(data=trainData,train$ast_pct~.)
summary(ast_pct1)
## 
## Call:
## lm(formula = train$ast_pct ~ ., data = trainData)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -0.76878 -0.11062  0.00657  0.12766  0.75467 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                1.682218   0.105939  15.879  < 2e-16 ***
## player_height             -0.059296   0.004749 -12.486  < 2e-16 ***
## player_weight             -0.115224   0.021660  -5.320 1.27e-07 ***
## draft_year                -0.012337   0.002741  -4.501 7.53e-06 ***
## draft_round                0.053562   0.021772   2.460   0.0141 *  
## draft_number              -0.159137   0.026008  -6.119 1.33e-09 ***
## continent_North_America   -0.179156   0.097011  -1.847   0.0651 .  
## continent_Central_America -0.176117   0.120717  -1.459   0.1449    
## continent_Europe          -0.097131   0.098658  -0.985   0.3251    
## continent_South_America   -0.161328   0.114249  -1.412   0.1582    
## continent_Oceania         -0.274971   0.124600  -2.207   0.0275 *  
## continent_Africa          -0.262993   0.104957  -2.506   0.0124 *  
## continent_Eurasia         -0.130081   0.113804  -1.143   0.2533    
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.192 on 1039 degrees of freedom
## Multiple R-squared:  0.4934,	Adjusted R-squared:  0.4875 
## F-statistic: 84.32 on 12 and 1039 DF,  p-value: < 2.2e-16


seasons1<-lm(data=trainData,train$seasons_played~.)
summary(seasons1)
## 
## Call:
## lm(formula = train$seasons_played ~ ., data = trainData)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -1.06513 -0.33394 -0.01582  0.32677  0.99858 
## 
## Coefficients: (1 not defined because of singularities)
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)                1.279132   0.230690   5.545 3.73e-08 ***
## player_height             -0.017877   0.010341  -1.729   0.0842 .  
## player_weight              0.053371   0.047167   1.132   0.2581    
## draft_year                -0.035187   0.005969  -5.895 5.05e-09 ***
## draft_round               -0.094822   0.047411  -2.000   0.0458 *  
## draft_number              -0.311967   0.056635  -5.508 4.57e-08 ***
## continent_North_America   -0.052060   0.211249  -0.246   0.8054    
## continent_Central_America  0.115804   0.262871   0.441   0.6596    
## continent_Europe          -0.048746   0.214837  -0.227   0.8205    
## continent_South_America    0.219572   0.248786   0.883   0.3777    
## continent_Oceania          0.080990   0.271328   0.298   0.7654    
## continent_Africa          -0.180768   0.228554  -0.791   0.4292    
## continent_Eurasia          0.232286   0.247818   0.937   0.3488    
## continent_Asia                   NA         NA      NA       NA    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.4181 on 1039 degrees of freedom
## Multiple R-squared:  0.1786,	Adjusted R-squared:  0.1691 
## F-statistic: 18.82 on 12 and 1039 DF,  p-value: < 2.2e-16
```
After this, we will try to find simpler models using regsubset.

```r
gp2 = summary(regsubsets(train$gp ~., data = trainData))

gp.mat = model.matrix(gp ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(gp2$obj, id=i)
        pred = gp.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$gp-pred)^2)
}
which.min(val.errors)
## [1] 2

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="gp error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-1.png" class="plot" /></div>

```r

layout(mat = matrix(1:47,2,2))
plot(gp2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "gp",outer = T,line = -1)
plot(gp2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(gp2$adjr2)
points(r2,gp2$adjr2[r2], col="red",cex=2,pch=20)
plot(gp2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(gp2$cp)
points(cp,gp2$cp[cp],col="red",cex=2,pch=20)
plot(gp2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(gp2$bic )
points(bic,gp2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-2.png" class="plot" /></div>

```r

pts2 = summary(regsubsets(train$pts ~., data = trainData))

pts.mat = model.matrix(pts ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(pts2$obj, id=i)
        pred = pts.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$pts-pred)^2)
}
which.min(val.errors)
## [1] 3

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="pts error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-3.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(pts2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "pts",outer = T,line = -1)
plot(pts2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(pts2$adjr2)
points(r2,pts2$adjr2[r2], col="red",cex=2,pch=20)
plot(pts2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(pts2$cp)
points(cp,pts2$cp[cp],col="red",cex=2,pch=20)
plot(pts2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(pts2$bic )
points(bic,pts2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-4.png" class="plot" /></div>

```r


reb2 = summary(regsubsets(train$reb ~., data = trainData))



reb.mat = model.matrix(reb ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(reb2$obj, id=i)
        pred = reb.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$reb-pred)^2)
}
which.min(val.errors)
## [1] 4

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="reb error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-5.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(reb2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "reb",outer = T,line = -1)
plot(reb2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(reb2$adjr2)
points(r2,reb2$adjr2[r2], col="red",cex=2,pch=20)
plot(reb2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(reb2$cp)
points(cp,reb2$cp[cp],col="red",cex=2,pch=20)
plot(reb2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(reb2$bic )
points(bic,reb2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-6.png" class="plot" /></div>

```r

ast2 = summary(regsubsets(train$ast ~., data = trainData))

ast.mat = model.matrix(ast ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(ast2$obj, id=i)
        pred = ast.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$ast-pred)^2)
}
which.min(val.errors)
## [1] 6

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="ast error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-7.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(ast2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "ast",outer = T,line = -1)
plot(ast2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(ast2$adjr2)
points(r2,ast2$adjr2[r2], col="red",cex=2,pch=20)
plot(ast2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(ast2$cp)
points(cp,ast2$cp[cp],col="red",cex=2,pch=20)
plot(ast2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(ast2$bic )
points(bic,ast2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-8.png" class="plot" /></div>

```r


net_rating2 = summary(regsubsets(train$net_rating~., data = trainData))


net_rating.mat = model.matrix(net_rating ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(net_rating2$obj, id=i)
        pred = net_rating.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$net_rating-pred)^2)
}
which.min(val.errors)
## [1] 6

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="net_rating error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-9.png" class="plot" /></div>

```r
layout(mat = matrix(1:4,2,2))
plot(reb2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "net_rating",outer = T,line = -1)
plot(reb2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(reb2$adjr2)
points(r2,reb2$adjr2[r2], col="red",cex=2,pch=20)
plot(reb2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(reb2$cp)
points(cp,reb2$cp[cp],col="red",cex=2,pch=20)
plot(reb2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(reb2$bic )
points(bic,reb2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-10.png" class="plot" /></div>

```r


oreb2 = summary(regsubsets(train$oreb_pct ~., data = trainData))

oreb.mat = model.matrix(oreb_pct ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(oreb2$obj, id=i)
        pred = oreb.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$oreb_pct-pred)^2)
}
which.min(val.errors)
## [1] 3

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="oreb_pct error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-11.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(oreb2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "oreb_pct",outer = T,line = -1)
plot(oreb2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(oreb2$adjr2)
points(r2,oreb2$adjr2[r2], col="red",cex=2,pch=20)
plot(oreb2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(oreb2$cp)
points(cp,oreb2$cp[cp],col="red",cex=2,pch=20)
plot(oreb2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(oreb2$bic )
points(bic,oreb2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-12.png" class="plot" /></div>

```r

dreb2 = summary(regsubsets(train$dreb_pct ~., data = trainData))

dreb.mat = model.matrix(dreb_pct ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(dreb2$obj, id=i)
        pred = dreb.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$dreb_pct-pred)^2)
}
which.min(val.errors)
## [1] 3

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="dreb_pct error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-13.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(dreb2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "dreb_pct",outer = T,line = -1)
plot(dreb2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(dreb2$adjr2)
points(r2,dreb2$adjr2[r2], col="red",cex=2,pch=20)
plot(dreb2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(dreb2$cp)
points(cp,dreb2$cp[cp],col="red",cex=2,pch=20)
plot(dreb2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(dreb2$bic )
points(bic,dreb2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-14.png" class="plot" /></div>

```r


usg2 = summary(regsubsets(train$usg_pct ~., data = trainData))

usg.mat = model.matrix(usg_pct ~.-gp-pts-reb-ast-net_rating-usg_pct-dreb_pct-oreb_pct-ts_pct-ast_pct-seasons_played, data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(usg2$obj, id=i)
        pred = usg.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$usg_pct-pred)^2)
}
which.min(val.errors)
## [1] 2

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="usg_pct error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-15.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(usg2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "usg_pct",outer = T,line = -1)
plot(usg2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(usg2$adjr2)
points(r2,usg2$adjr2[r2], col="red",cex=2,pch=20)
plot(usg2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(usg2$cp)
points(cp,usg2$cp[cp],col="red",cex=2,pch=20)
plot(usg2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(usg2$bic )
points(bic,usg2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-16.png" class="plot" /></div>

```r

ts2 = summary(regsubsets(train$ts_pct ~., data = trainData))
ts.mat = model.matrix(ts_pct ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(ts2$obj, id=i)
        pred = ts.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$ts_pct-pred)^2)
}
which.min(val.errors)
## [1] 1

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="ts_pct error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-17.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(ts2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "ts_pct",outer = T,line = -1)
plot(ts2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(ts2$adjr2)
points(r2,ts2$adjr2[r2], col="red",cex=2,pch=20)
plot(ts2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(ts2$cp)
points(cp,ts2$cp[cp],col="red",cex=2,pch=20)
plot(ts2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(ts2$bic )
points(bic,ts2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-18.png" class="plot" /></div>

```r

ast_pct2 = summary(regsubsets(train$ast_pct ~., data = trainData))

ast_pct.mat = model.matrix(ast_pct ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(ast_pct2$obj, id=i)
        pred = ast_pct.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$ast_pct-pred)^2)
}
which.min(val.errors)
## [1] 5

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="ast_pct error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-19.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(ast_pct2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "ast_pct",outer = T,line = -1)
plot(ast_pct2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(ast_pct2$adjr2)
points(r2,ast_pct2$adjr2[r2], col="red",cex=2,pch=20)
plot(ast_pct2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(ast_pct2$cp)
points(cp,ast_pct2$cp[cp],col="red",cex=2,pch=20)
plot(ast_pct2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(ast_pct2$bic )
points(bic,ast_pct2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-20.png" class="plot" /></div>

```r


seasons2 = summary(regsubsets(train$seasons_played ~., data = trainData))

seasons.mat = model.matrix(seasons_played ~., data=test)
val.errors = rep(NA,8)
for (i in 1:8){
        coefi = coef(seasons2$obj, id=i)
        pred = seasons.mat[,names(coefi)]%*%coefi
        val.errors[i] = mean((test$seasons_played-pred)^2)
}
which.min(val.errors)
## [1] 3

verr <- as.data.frame(val.errors);  names(verr) <- "err"
index <- c(1:nrow(verr))
verr <- cbind.data.frame(verr,index)
        

ggplot(verr)+geom_point(aes(x=index, y=err))+geom_line(aes(x=index, y=err))+labs(title="seasons_played error",x="number of variables",y= "error")
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-21.png" class="plot" /></div>

```r

layout(mat = matrix(1:4,2,2))
plot(seasons2$rss ,xlab="Number of Variables ",ylab="RSS",type="l")
title(main = "seasons_played",outer = T,line = -1)
plot(seasons2$adjr2 ,xlab="Number of Variables ", ylab="Adjusted RSq",type="l")
r2<-which.max(seasons2$adjr2)
points(r2,seasons2$adjr2[r2], col="red",cex=2,pch=20)
plot(seasons2$cp ,xlab="Number of Variables ",ylab="Cp", type='l')
cp<-which.min(seasons2$cp)
points(cp,seasons2$cp[cp],col="red",cex=2,pch=20)
plot(seasons2$bic ,xlab="Number of Variables ",ylab="BIC",type='l')
bic<-which.min(seasons2$bic )
points(bic,seasons2$bic [bic],col="red",cex=2,pch=20)
```

<div class="rimage center"><img src="fig/unnamed-chunk-13-22.png" class="plot" /></div>

Let's test the models we found:

```r
#par(mfrow=c(2,2))
gpModel<-coef(gp2$obj,5)
gpModel
##       (Intercept)     player_height        draft_year 
##        5.19106325       -0.07271753       -0.05498522 
##       draft_round      draft_number continent_Eurasia 
##        0.32168272       -1.99352339        0.90235783
gpModel<-lm(train$gp~player_height+draft_year+draft_round+draft_number+continent_Eurasia,data=trainData)
plot(gpModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-1.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-2.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-3.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-4.png" class="plot" /></div>

```r

ptsModel<-coef(train$pts2$obj,4)
ptsModel
## NULL
ptsModel<-lm(train$pts~player_height+draft_year+draft_round+draft_number,data=trainData)
plot(ptsModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-5.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-6.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-7.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-8.png" class="plot" /></div>

```r

rebModel<-coef(reb2$obj,5)
rebModel
##       (Intercept)     player_weight        draft_year 
##        0.59632824        0.16564306       -0.01081418 
##       draft_round      draft_number continent_Oceania 
##        0.08036713       -0.34931847       -0.26511059
rebModel<-lm(train$reb~player_height+player_weight+draft_year+draft_round+draft_number,data=trainData)
plot(rebModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-9.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-10.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-11.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-12.png" class="plot" /></div>

```r


astModel<-coef(ast2$obj,5)
astModel
##   (Intercept) player_height player_weight    draft_year   draft_round 
##    1.13390770   -0.03870278   -0.07538000   -0.01440411    0.08224243 
##  draft_number 
##   -0.27739043
astModel<-lm(train$ast~player_height+draft_year+draft_round+draft_number+continent_North_America,data=trainData)
plot(astModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-13.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-14.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-15.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-16.png" class="plot" /></div>

```r

net_ratingModel<-coef(net_rating2$obj,6)
net_ratingModel
##       (Intercept)     player_height     player_weight 
##        74.8371672        -0.6632508         1.9057191 
##        draft_year       draft_round  continent_Europe 
##        -0.4135156        -1.4630335         1.8920326 
## continent_Oceania 
##       -11.5140734
net_ratingModel<-lm(train$net_rating~player_height+player_weight+draft_year+draft_round+continent_North_America+continent_Oceania,data=trainData)
plot(net_ratingModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-17.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-18.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-19.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-20.png" class="plot" /></div>

```r

orebModel<-coef(oreb2$obj,5)
orebModel
##               (Intercept)             player_height 
##               -0.18592481                0.04906153 
##             player_weight                draft_year 
##                0.32761299               -0.01001963 
## continent_Central_America            continent_Asia 
##                0.24050094               -0.29134783
orebModel<-lm(train$oreb_pct~player_height+player_weight+draft_number+continent_Europe+continent_Asia,data=trainData)
plot(orebModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-21.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-22.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-23.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-24.png" class="plot" /></div>

```r

drebModel<-coef(dreb2$obj,7)
drebModel
##               (Intercept)             player_height 
##                0.24953116                0.07134852 
##             player_weight                draft_year 
##                0.36185695                0.01118825 
##              draft_number continent_Central_America 
##               -0.11684434                0.23712309 
##         continent_Oceania          continent_Africa 
##               -0.63068452                0.16649151
drebModel<-lm(train$dreb_pct~player_height+player_weight+draft_number+continent_North_America+continent_Europe+continent_Asia,data=trainData)
plot(drebModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-25.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-26.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-27.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-28.png" class="plot" /></div>

```r

usgModel<-coef(usg2$obj,2)
usgModel
##   (Intercept) player_height  draft_number 
##    2.80774687   -0.03123589   -0.28265430
usgModel<-lm(train$usg_pct~player_height+draft_number,data=trainData)
plot(usgModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-29.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-30.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-31.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-32.png" class="plot" /></div>

```r

tsModel<-coef(ts2$obj,5)
tsModel
##      (Intercept)    player_height    player_weight       draft_year 
##       9.63222553      -0.13525960       0.58661642      -0.05014824 
##     draft_number continent_Europe 
##      -0.58742740       0.48971813
tsModel<-lm(train$ts_pct~player_height+player_weight+draft_year+draft_number+continent_Europe,data=trainData)
plot(tsModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-33.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-34.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-35.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-36.png" class="plot" /></div>

```r

ast_pctModel<-coef(ast_pct2$obj,5)
ast_pctModel
##      (Intercept)    player_height    player_weight       draft_year 
##       1.49179389      -0.05884100      -0.11682507      -0.01369095 
##     draft_number continent_Europe 
##      -0.10629106       0.08207140
ast_pctModel<-lm(train$ast_pct~player_height+player_weight+draft_year+draft_number+continent_Europe,data=trainData)
plot(ast_pctModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-37.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-38.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-39.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-40.png" class="plot" /></div>

```r

seasonsModel<-coef(seasons2$obj,5)
seasonsModel
##             (Intercept)              draft_year 
##              1.19124572             -0.03594586 
##             draft_round            draft_number 
##             -0.09640284             -0.30383846 
## continent_South_America       continent_Eurasia 
##              0.26338650              0.26563747
seasonsModel<-lm(train$seasons_played~draft_year+draft_number+continent_South_America+continent_Africa,data=trainData)
plot(seasonsModel)
```

<div class="rimage center"><img src="fig/unnamed-chunk-14-41.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-42.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-43.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-14-44.png" class="plot" /></div>
We can see that most of the models' residuals aren't normal. Only the models for pts reb and ast seems to have normal residuals. Let's check it with the shapiro-wilk normality test:

```r
shapiro.test(gpModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  gpModel$residuals
## W = 0.98417, p-value = 2.853e-09
shapiro.test(ptsModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  ptsModel$residuals
## W = 0.99805, p-value = 0.2636
shapiro.test(rebModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  rebModel$residuals
## W = 0.99567, p-value = 0.004555
shapiro.test(astModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  astModel$residuals
## W = 0.997, p-value = 0.0452
shapiro.test(net_ratingModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  net_ratingModel$residuals
## W = 0.84758, p-value < 2.2e-16
shapiro.test(orebModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  orebModel$residuals
## W = 0.96942, p-value = 4.369e-14
shapiro.test(drebModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  drebModel$residuals
## W = 0.92656, p-value < 2.2e-16
shapiro.test(usgModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  usgModel$residuals
## W = 0.96722, p-value = 1.167e-14
shapiro.test(tsModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  tsModel$residuals
## W = 0.85699, p-value < 2.2e-16
shapiro.test(ast_pctModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  ast_pctModel$residuals
## W = 0.9846, p-value = 4.321e-09
shapiro.test(seasonsModel$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  seasonsModel$residuals
## W = 0.98535, p-value = 8.893e-09
```
We can see that only the pts model passed the test, and the reb model is really close to pass it. So we will try to find a bigger models in order to achieve normal residuals.

```r
gp3<-lm(data=trainData,train$gp~.^6)
plot(gp3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-1.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-2.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-3.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-4.png" class="plot" /></div>

```r

pts3<-lm(data=trainData,train$pts~.)
plot(gp3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-5.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-6.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-7.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-8.png" class="plot" /></div>

```r

reb3<-lm(data=trainData,train$reb~.^5)
plot(gp3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-9.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-10.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-11.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-12.png" class="plot" /></div>

```r

ast3<-lm(data=trainData,train$ast~.)
plot(ast3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-13.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-14.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-15.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-16.png" class="plot" /></div>

```r

net_rating3<-lm(data=trainData,train$net_rating~.^6)
plot(net_rating3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-17.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-18.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-19.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-20.png" class="plot" /></div>

```r

oreb3<-lm(data=trainData,train$oreb_pct~.^6)
plot(oreb3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-21.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-22.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-23.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-24.png" class="plot" /></div>

```r

dreb3<-lm(data=trainData,train$dreb_pct~.^6)
plot(dreb3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-25.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-26.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-27.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-28.png" class="plot" /></div>

```r

usg3<-lm(data=trainData,train$usg_pct~.^6)
plot(usg3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-29.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-30.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-31.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-32.png" class="plot" /></div>

```r

ts3<-lm(data=trainData,train$ts_pct~.^6)
plot(ts3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-33.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-34.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-35.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-36.png" class="plot" /></div>

```r

ast_pct3<-lm(data=trainData,train$ast_pct~.^6)
plot(ast_pct3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-37.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-38.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-39.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-40.png" class="plot" /></div>

```r

seasons3<-lm(data=trainData,train$seasons_played~.^6)
plot(seasons3)
```

<div class="rimage center"><img src="fig/unnamed-chunk-16-41.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-42.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-43.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-16-44.png" class="plot" /></div>

```r

reb5<-lm(data=trainData,train$reb~poly(draft_number,2)+continent_North_America+draft_number*player_weight+draft_number*continent_North_America+player_weight*player_height+draft_number*player_height+draft_number*player_weight*player_height+draft_round*draft_number*player_weight*continent_North_America)

shapiro.test(gp3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  gp3$residuals
## W = 0.99064, p-value = 3.093e-06
shapiro.test(pts3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  pts3$residuals
## W = 0.99802, p-value = 0.2537
shapiro.test(reb3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  reb3$residuals
## W = 0.9972, p-value = 0.06319
shapiro.test(ast3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  ast3$residuals
## W = 0.99751, p-value = 0.1079
shapiro.test(net_rating3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  net_rating3$residuals
## W = 0.85561, p-value < 2.2e-16
shapiro.test(oreb3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  oreb3$residuals
## W = 0.96773, p-value = 1.573e-14
shapiro.test(dreb3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  dreb3$residuals
## W = 0.91684, p-value < 2.2e-16
shapiro.test(usg3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  usg3$residuals
## W = 0.95869, p-value < 2.2e-16
shapiro.test(ts3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  ts3$residuals
## W = 0.85033, p-value < 2.2e-16
shapiro.test(ast_pct3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  ast_pct3$residuals
## W = 0.97733, p-value = 9.331e-12
shapiro.test(seasons3$residuals)
## 
## 	Shapiro-Wilk normality test
## 
## data:  seasons3$residuals
## W = 0.99337, p-value = 0.0001242
```
Now we can see that reb and ast also have normal residuals. We gave up on finding bigger models for the rest of the parameters because the model building with high degrees like this takes many time.


We will try to predict using the models we found

```r
gpPred<-predict(gpModel,newdata=test)
ptsPred<-predict(ptsModel,newdata=test)
rebPred<-predict(rebModel,newdata=test)
astPred<-predict(astModel,newdata=test)
netPred<-predict(net_ratingModel,newdata=test)
orebPred<-predict(orebModel,newdata=test)
drebPred<-predict(drebModel,newdata=test)
usgPred<-predict(usgModel,newdata=test)
tsPred<-predict(tsModel,newdata=test)
astPctPred<-predict(ast_pctModel,newdata=test)
seasonsPred<-predict(seasonsModel,newdata=test)

gpPred2<-predict(gp1,newdata=test)
ptsPred2<-predict(pts1,newdata=test)
rebPred2<-predict(reb1,newdata=test)
astPred2<-predict(ast1,newdata=test)
netPred2<-predict(net_rating1,newdata=test)
orebPred2<-predict(oreb1,newdata=test)
drebPred2<-predict(dreb1,newdata=test)
usgPred2<-predict(usg1,newdata=test)
tsPred2<-predict(ts1,newdata=test)
astPctPred2<-predict(ast_pct1,newdata=test)
seasonsPred2<-predict(seasons1,newdata=test)


gpPred3<-predict(gp3,newdata=test)
ptsPred3<-predict(pts3,newdata=test)
rebPred3<-predict(reb3,newdata=test)
astPred3<-predict(ast3,newdata=test)
netPred3<-predict(net_rating3,newdata=test)
orebPred3<-predict(oreb3,newdata=test)
drebPred3<-predict(dreb3,newdata=test)
usgPred3<-predict(usg3,newdata=test)
tsPred3<-predict(ts3,newdata=test)
astPctPred3<-predict(ast_pct3,newdata=test)
seasonsPred3<-predict(seasons3,newdata=test)



predicted<-data.frame(gpPred,ptsPred,rebPred,astPred,netPred,orebPred,drebPred,usgPred,tsPred,astPctPred,seasonsPred)


predicted2<-data.frame(gpPred2,ptsPred2,rebPred2,astPred2,netPred2,orebPred2,drebPred2,usgPred2,tsPred2,astPctPred2,seasonsPred2)

predicted3<-data.frame(gpPred3,ptsPred3,rebPred3,astPred3,netPred3,orebPred3,drebPred3,usgPred3,tsPred3,astPctPred3,seasonsPred3)



  plotData1=data.frame(test,predicted)
  plotData2=data.frame(test,predicted2)
  plotData3=data.frame(test,predicted3)
```
In order to check our prediction fit, we will plot the true values of the test data VS. the predicted data, we would like to see a linear connection with slope of 1 and intercept 0.

```r
ggplot(plotData1,aes(y=gp,x=gpPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=gp,x=gpPred2),color="blue") +geom_smooth(data = plotData2,aes(y=gp,x=gpPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=gp,x=gpPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(1,5)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-1.png" class="plot" /></div>

```r
  


ggplot(plotData1,aes(y=pts,x=ptsPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=pts,x=ptsPred2),color="blue") +geom_smooth(data = plotData2,aes(y=pts,x=ptsPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=pts,x=ptsPred3),color="green")+geom_smooth(method=lm,color="green",se=F)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-2.png" class="plot" /></div>

```r


ggplot(plotData1,aes(y=reb,x=rebPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=reb,x=rebPred2),color="blue") +geom_smooth(data = plotData2,aes(y=reb,x=rebPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=reb,x=rebPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(0,1)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-3.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=ast,x=astPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=ast,x=astPred2),color="blue") +geom_smooth(data = plotData2,aes(y=ast,x=astPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=ast,x=astPred3),color="green")+geom_smooth(method=lm,color="green",se=F)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-4.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=net_rating,x=netPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=net_rating,x=netPred2),color="blue") +geom_smooth(data = plotData2,aes(y=net_rating,x=netPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=net_rating,x=netPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(65,75)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-5.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=oreb_pct,x=orebPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=oreb_pct,x=orebPred2),color="blue") +geom_smooth(data = plotData2,aes(y=oreb_pct,x=orebPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=oreb_pct,x=orebPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(0,2)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-6.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=dreb_pct,x=drebPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=dreb_pct,x=drebPred2),color="blue") +geom_smooth(data = plotData2,aes(y=dreb_pct,x=drebPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=dreb_pct,x=drebPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(0,2.5)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-7.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=usg_pct,x=usgPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=usg_pct,x=usgPred2),color="blue") +geom_smooth(data = plotData2,aes(y=usg_pct,x=usgPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=usg_pct,x=usgPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(2,2.75)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-8.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=ts_pct,x=tsPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=ts_pct,x=tsPred2),color="blue") +geom_smooth(data = plotData2,aes(y=ts_pct,x=tsPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=ts_pct,x=tsPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(7.5,10)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-9.png" class="plot" /></div>

```r


ggplot(plotData1,aes(y=ast_pct,x=astPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=ast_pct,x=astPred2),color="blue") +geom_smooth(data = plotData2,aes(y=ast_pct,x=astPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=ast_pct,x=astPred3),color="green")+geom_smooth(method=lm,color="green",se=F)
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-10.png" class="plot" /></div>

```r

ggplot(plotData1,aes(y=seasons_played,x=seasonsPred))+geom_point(color="red")+geom_smooth(method = lm,color="red",se=F) + geom_point(data=plotData2,aes(y=seasons_played,x=seasonsPred2),color="blue") +geom_smooth(data = plotData2,aes(y=seasons_played,x=seasonsPred2),method = lm,color="blue",se=F)+geom_point(data=plotData3,aes(y=seasons_played,x=seasonsPred3),color="green")+geom_smooth(method=lm,color="green",se=F)+xlim(0,1)+labs(colour=c("name1","name2,name3"))
```

<div class="rimage center"><img src="fig/unnamed-chunk-18-11.png" class="plot" /></div>
red- model 1
blue- model 2
green- model 3

We can see that most of the models predicted pretty well.


```r
gpError1<-sum((test$gp-plotData1$gpPred)^2)
gpError2<-sum((test$gp-plotData2$gpPred)^2)
gpError3<-sum((test$gp-plotData3$gpPred)^2)

ptsError1<-sum((test$pts-plotData1$ptsPred)^2)
ptsError2<-sum((test$pts-plotData2$ptsPred)^2)
ptsError3<-sum((test$pts-plotData3$ptsPred)^2)

rebError1<-sum((test$reb-plotData1$rebPred)^2)
rebError2<-sum((test$reb-plotData2$rebPred)^2)
rebError3<-sum((test$reb-plotData3$rebPred)^2)

astError1<-sum((test$ast-plotData1$astPred)^2)
astError2<-sum((test$ast-plotData2$astPred)^2)
astError3<-sum((test$ast-plotData3$astPred)^2)

netError1<-sum((test$net_rating-plotData1$netPred)^2)
netError2<-sum((test$net_rating-plotData2$netPred)^2)
netError3<-sum((test$net_rating-plotData3$netPred)^2)

orebError1<-sum((test$oreb_pct-plotData1$orebPred)^2)
orebError2<-sum((test$oreb_pct-plotData2$orebPred)^2)
orebError3<-sum((test$oreb_pct-plotData3$orebPred)^2)

drebError1<-sum((test$dreb_pct-plotData1$drebPred)^2)
drebError2<-sum((test$dreb_pct-plotData2$drebPred)^2)
drebError3<-sum((test$dreb_pct-plotData3$drebPred)^2)

usgError1<-sum((test$usg_pct-plotData1$usgPred)^2)
usgError2<-sum((test$usg_pct-plotData2$usgPred)^2)
usgError3<-sum((test$usg_pct-plotData3$usgPred)^2)

tsError1<-sum((test$ts_pct-plotData1$tsPred)^2)
tsError2<-sum((test$ts_pct-plotData2$tsPred)^2)
tsError3<-sum((test$ts_pct-plotData3$tsPred)^2)

astError1<-sum((test$ast_pct-plotData1$astPred)^2)
astError2<-sum((test$ast_pct-plotData2$astPred)^2)
astError3<-sum((test$ast_pct-plotData3$astPred)^2)

seasonsError1<-sum((test$seasons_played-plotData1$seasonsPred)^2)
seasonsError2<-sum((test$seasons_played-plotData2$seasonsPred)^2)
seasonsError3<-sum((test$seasons_played-plotData3$seasonsPred)^2)
```
So we choose the models with the minimal error.

Now we will do the same after the rescale to make sure we are right.

```r
finalPred1<-1:11
finalPred2<-1:11
finalPred3<-1:11

finalPred1<-rescale(scaleData = predicted,lambda = lambda[6:16],min = minData[6:16],sd=dataSD[6:16] ,mean = dataMean[6:16])
finalPred2<-rescale(scaleData = predicted2,lambda = lambda[6:16],min = minData[6:16],sd=dataSD[6:16] ,mean = dataMean[6:16])
finalPred3<-rescale(scaleData = predicted3,lambda = lambda[6:16],min = minData[6:16],sd=dataSD[6:16] ,mean = dataMean[6:16])

for (i in 1:11) {
  hist(as.numeric(finalPred1[,i]), main=colnames(finalPred1)[i],probability = F,col = myColors[5],border =myColors[7])
  
}
```

<div class="rimage center"><img src="fig/unnamed-chunk-20-1.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-2.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-3.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-4.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-5.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-6.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-7.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-8.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-9.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-10.png" class="plot" /></div>
<div class="rimage center"><img src="fig/unnamed-chunk-20-11.png" class="plot" /></div>


```r
#RMSE
gpRmse1<-RMSE(finalPred1$gpPred,data$gp[-train_ind])
gpRmse1
## [1] 23.58515
gpRmse2<-RMSE(finalPred2$gpPred2,data$gp[-train_ind])
gpRmse2
## [1] 23.55971
  
ptsRmse1<-RMSE(finalPred1$ptsPred,data$pts[-train_ind])
ptsRmse1
## [1] 4.11394
ptsRmse2<-RMSE(finalPred2$ptsPred2,data$pts[-train_ind])
ptsRmse2
## [1] 4.110499

rebRmse1<-RMSE(finalPred1$rebPred,data$reb[-train_ind])
rebRmse1
## [1] 1.704305
rebRmse2<-RMSE(finalPred2$rebPred2,data$reb[-train_ind])
rebRmse2
## [1] 1.707985

astRmse1<-RMSE(finalPred1$astPred,data$ast[-train_ind])
astRmse1
## [1] 1.294318
astRmse2<-RMSE(finalPred2$astPred2,data$ast[-train_ind])
astRmse2
## [1] 1.442192

netRmse1<-RMSE(finalPred1$netPred,data$net_rating[-train_ind])
netRmse1
## [1] 13.41256
netRmse2<-RMSE(finalPred2$netPred2,data$net_rating[-train_ind])
netRmse2
## [1] 13.42485

orebRmse1<-RMSE(finalPred1$orebPred,data$oreb_pct[-train_ind])
orebRmse1
## [1] 0.03487187
orebRmse2<-RMSE(finalPred2$orebPred2,data$oreb_pct[-train_ind])
orebRmse2
## [1] 0.03530538

drebRmse1<-RMSE(finalPred1$drebPred,data$dreb_pct[-train_ind])
drebRmse1
## [1] 0.04157689
drebRmse2<-RMSE(finalPred2$drebPred2,data$dreb_pct[-train_ind])
drebRmse2
## [1] 0.04165254

usgRmse1<-RMSE(finalPred1$usgPred,data$usg_pct[-train_ind])
usgRmse1
## [1] 0.05690907
usgRmse2<-RMSE(finalPred2$usgPred2,data$usg_pct[-train_ind])
usgRmse2
## [1] 0.05706711

tsRmse1<-RMSE(finalPred1$tsPred,data$ts_pct[-train_ind])
tsRmse1
## [1] 0.08803034
tsRmse2<-RMSE(finalPred2$tsPred2,data$ts_pct[-train_ind])
tsRmse2
## [1] 0.08826821

astPctRmse1<-RMSE(finalPred1$astPctPred,data$ast_pct[-train_ind])
astPctRmse1
## [1] 0.07077042
astPctRmse2<-RMSE(finalPred2$astPctPred2,data$ast_pct[-train_ind])
astPctRmse2
## [1] 0.07123219

seasonsRmse1<-RMSE(finalPred1$seasonsPred,data$seasons_played[-train_ind])
seasonsRmse1
## [1] 4.112678
seasonsRmse2<-RMSE(finalPred2$seasonsPred2,data$seasons_played[-train_ind])
seasonsRmse2
## [1] 4.091314
```

In coclusion, we found good prediction models for pts, ast and reb.
We can use the models for the rest of the parameters to evaluate their value' but because their residuals aren't normal, it wont be the most accurate predictions.

the models are:

```r
#gp- gpModel
gpModel$coefficients
##       (Intercept)     player_height        draft_year 
##        5.19106325       -0.07271753       -0.05498522 
##       draft_round      draft_number continent_Eurasia 
##        0.32168272       -1.99352339        0.90235783

#pts- ptsModel
ptsModel$coefficients
##   (Intercept) player_height    draft_year   draft_round  draft_number 
##    1.16649457   -0.01972101   -0.01421754    0.10612354   -0.43891213

#reb- rebModel
rebModel$coefficients
##   (Intercept) player_height player_weight    draft_year   draft_round 
##   0.601408555   0.007593546   0.134790287  -0.011701353   0.077960906 
##  draft_number 
##  -0.345880060

#ast- astModel
astModel$coefficients
##             (Intercept)           player_height 
##              1.15269753             -0.05393066 
##              draft_year             draft_round 
##             -0.01498125              0.08307904 
##            draft_number continent_North_America 
##             -0.28232311             -0.04831216

#net_rating- net_ratingModel
net_ratingModel$coefficients
##             (Intercept)           player_height 
##              76.5359682              -0.6739106 
##           player_weight              draft_year 
##               1.8746514              -0.4252284 
##             draft_round continent_North_America 
##              -1.4529150              -1.5083844 
##       continent_Oceania 
##             -12.9519327

#oreb_pct- orebModel
orebModel$coefficients
##      (Intercept)    player_height    player_weight     draft_number 
##      -0.28252390       0.05334935       0.31899772       0.01483360 
## continent_Europe   continent_Asia 
##      -0.08447395      -0.31639217

#dreb_pct- drebModel
drebModel$coefficients
##             (Intercept)           player_height 
##              0.40989205              0.07362419 
##           player_weight            draft_number 
##              0.34944369             -0.11963460 
## continent_North_America        continent_Europe 
##             -0.07142438             -0.02468792 
##          continent_Asia 
##             -0.31190811

#usg_pct- usgModel
usgModel$coefficients
##   (Intercept) player_height  draft_number 
##    2.80774687   -0.03123589   -0.28265430

#ts_pct- tsModel
tsModel$coefficients
##      (Intercept)    player_height    player_weight       draft_year 
##       9.63222553      -0.13525960       0.58661642      -0.05014824 
##     draft_number continent_Europe 
##      -0.58742740       0.48971813

#ast_pct- ast_pctModel
ast_pctModel$coefficients
##      (Intercept)    player_height    player_weight       draft_year 
##       1.49179389      -0.05884100      -0.11682507      -0.01369095 
##     draft_number continent_Europe 
##      -0.10629106       0.08207140

#seasons_played- seasonsModel
seasonsModel$coefficients
##             (Intercept)              draft_year 
##              1.21146257             -0.03309772 
##            draft_number continent_South_America 
##             -0.39892780              0.27725649 
##        continent_Africa 
##             -0.15123284
```
We need to scale the data using the scaleD function that we created, and rescale the prediction using the rescale function we created.
